package br.com.tcc.safeson.safeson.models;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.InputStream;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;

public class User implements Serializable {
    public String nome;
    public String email;
    public String imgUrl;
    public String socialId;
    public boolean created;

    public User(String nome, String email, String imgUrl, String socialId, boolean created) {
        this.nome = nome;
        this.email = email;
        this.imgUrl = imgUrl;
        this.socialId = socialId;
        this.created = created;
    }

    public Bitmap getImg() {
        try {
            URL myFileUrl = new URL(this.imgUrl);
            HttpURLConnection conn =
                    (HttpURLConnection) myFileUrl.openConnection();
            conn.setDoInput(true);
            conn.connect();

            InputStream is = conn.getInputStream();
            return BitmapFactory.decodeStream(is);

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
