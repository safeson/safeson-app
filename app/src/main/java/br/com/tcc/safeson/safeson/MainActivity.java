package br.com.tcc.safeson.safeson;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.squareup.picasso.Picasso;

import br.com.tcc.safeson.safeson.actions.BaseMessage;
import br.com.tcc.safeson.safeson.actions.EventLogin.EventLoginGoogleMessage;
import br.com.tcc.safeson.safeson.fragment.FriendsFragment;
import br.com.tcc.safeson.safeson.fragment.MapFragment;
import br.com.tcc.safeson.safeson.models.User;
import br.com.tcc.safeson.safeson.socket.SocketConnection;
import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, GoogleApiClient.OnConnectionFailedListener, FriendsFragment.OnFragmentInteractionListener, MapFragment.OnFragmentInteractionListener {
    private static final String TAG_HOME = "map";
    private static final String TAG_FRINEDS = "amigos";
    private static final String TAG_GROUPS = "grupos";
    private static final String TAG_NOTIFICATIONS = "notificações";
    private static final String TAG_SETTINGS = "configuraçoes";
    private static final String TAG_ABOUT = "sobre";
    private static final String TAG_REPORTS = "acontecimentos";
    private static final String TAG_EXIT = "sair";
    public static String CURRENT_TAG = TAG_HOME;

    MapFragment mapFrangment;


    private NavigationView navigationView;
    private DrawerLayout drawer;
    private View headerView;
    private Toolbar toolbar;
    private FloatingActionButton fab;

    public static int navItemIndex = 0;


    private GoogleApiClient googleApiClient;

    private SocketConnection socket = new SocketConnection();

    private FragmentManager fragmentManager;
    private Handler mHandler;
    TextView txt;
    CircleImageView userImg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        GoogleSignInOptions googleSignInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build();
        googleApiClient = new GoogleApiClient.Builder(this).enableAutoManage(this, this).addApi(Auth.GOOGLE_SIGN_IN_API, googleSignInOptions).build();
        socket.create();
        socket.connect();

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mHandler = new Handler();

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                navItemIndex = 0;
                loadHomeFragment();
            }
        });

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        mapFrangment = new MapFragment();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        headerView = navigationView.getHeaderView(0);
        txt = (TextView) headerView.findViewById(R.id.txtUserName);
        userImg = (CircleImageView) headerView.findViewById(R.id.imgUser);


        fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();

        transaction.add(R.id.container, mapFrangment, "MapsFragment");

        transaction.commitAllowingStateLoss();
    }


    @Override
    protected void onStart() {
        super.onStart();

        OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(googleApiClient);
        if (opr.isDone()) {
            GoogleSignInResult result = opr.get();
            handleSingInResult(result);
        } else {
            opr.setResultCallback(googleSignInResult -> handleSingInResult(googleSignInResult)
            );
        }
    }

    private void handleSingInResult(GoogleSignInResult result) {
        if (result.isSuccess()) {
            GoogleSignInAccount account = result.getSignInAccount();
            System.out.println("Tokeeeeen: "+account.getIdToken());
            socket.sendSingleMessage(
                    new BaseMessage("event.login.google",
                            new EventLoginGoogleMessage(account.getIdToken())
                    ), User.class
            ).subscribe(
                    (res) -> {
                        User userResult = (User) res.result;

                        txt.setText("Bem vindo, " + userResult.nome);
                        Picasso.get().load(userResult.imgUrl).into(userImg);

                    },
                    (err) -> {
                        System.out.println(err);
                    }
            );

        } else {
            logInScreen();
        }
    }
    private void logInScreen(){
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.friends:
                    navItemIndex = 1;
                    CURRENT_TAG = TAG_FRINEDS;
                break;
            case R.id.exit:
                Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback(new ResultCallback<Status>() {
                    @Override
                    public void onResult(@NonNull Status status) {
                        if (status.isSuccess()){
                            logInScreen();

                        }else {
                            Log.e(CURRENT_TAG, "onResult: " +"Err disconnect" );
                        }
                    }
                });

                break;

            default:
                navItemIndex = 0;
                break;
        }
        loadHomeFragment();
        return true;
    }

    private void loadHomeFragment() {
        if (getSupportFragmentManager().findFragmentByTag(CURRENT_TAG) != null) {
            drawer.closeDrawers();

            // show or hide the fab button
            toggleFab();
            return;
        }
        Runnable mPendingRunnable = new Runnable() {
            @Override
            public void run() {
                // update the main content by replacing fragments
                Fragment fragment = getHomeFragment();
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
                        android.R.anim.fade_out);
                fragmentTransaction.replace(R.id.container, fragment, CURRENT_TAG);
                fragmentTransaction.commitAllowingStateLoss();
            }
        };

        if (mPendingRunnable != null) {
            mHandler.post(mPendingRunnable);
        }

        toggleFab();

        drawer.closeDrawers();

        invalidateOptionsMenu();

    }
    private Fragment getHomeFragment() {
        switch (navItemIndex) {
            case 0:
                return mapFrangment;
            case 1:
                FriendsFragment friendsFragment = new FriendsFragment();
                return friendsFragment;

            default:
                return mapFrangment;
        }
    }

    private void toggleFab() {
        if (navItemIndex != 0)
            fab.show();
        else
            fab.hide();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onFragmentInteraction(Uri uri) {
    }
}
