package br.com.tcc.safeson.safeson;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;

import br.com.tcc.safeson.safeson.actions.BaseMessage;
import br.com.tcc.safeson.safeson.actions.EventLogin.EventLoginGoogleMessage;
import br.com.tcc.safeson.safeson.models.User;
import br.com.tcc.safeson.safeson.socket.SocketConnection;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity {
    private static final int RC_SIGN_IN = 9001;

    private GoogleSignInClient mGoogleSignInClient;
    private GoogleSignInOptions gso;
    private SocketConnection socket = new SocketConnection();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getWindow().setStatusBarColor(Color.GRAY);

        ButterKnife.bind(this);

        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.server_client_id))
                .requestEmail()
                .build();
        socket.create();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                GoogleSignInAccount account = task.getResult(ApiException.class);

                socket.sendSingleMessage(
                        new BaseMessage("event.login.google",
                                new EventLoginGoogleMessage(account.getIdToken())
                        ), User.class
                ).subscribe(
                        (res) -> {
                            User result = (User) res.result;

                            Intent intent = new Intent(this, MainActivity.class);
                            intent.putExtra("user", result);
                            startActivity(intent);
                        },
                        (err) -> {
                            System.out.println(err);
                        }
                );
            } catch (ApiException e) {
                System.out.println("Error - Google sign in " + e);
            }
        }
    }

    @OnClick(R.id.sign_google) void onClickGoogle() {
        socket.connect();
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }
}
