package br.com.tcc.safeson.safeson.socket;

import com.google.gson.Gson;
import com.navin.flintstones.rxwebsocket.RxWebsocket;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

import br.com.tcc.safeson.safeson.actions.BaseMessage;
import io.reactivex.Single;
import io.reactivex.SingleEmitter;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class SocketConnection {
    private String url = "ws://192.168.1.103:8080";
    private RxWebsocket websocket;

    // Para direcionar os eventos ouvidos para o correspondente que chamou
    private HashMap<String, SingleEmitter<ResultObject>> singles;
    private HashMap<String, Class> singlesParser;

    public void create() {
        websocket = new RxWebsocket.Builder()
            .addConverterFactory(WebSocketConverterFactory.create())
            //.addReceiveInterceptor(data -> data)
            .build(url);

        singles = new HashMap<>();
        singlesParser = new HashMap<>();
        listen();
    }

    /**
     * Send a WebSocket message with a Single return value
     * @param data Data to sent
     * @return Single emmiter object
     */
    public Single<ResultObject> sendSingleMessage(BaseMessage data) {
        return Single.create(emitter -> emitSingleWebsocketMessage(emitter, data));
    }

    /**
     * Send a WebSocket message with a Single return value
     * @param data Data to send
     * @param parseResult Objeto de resultado
     * @return Single emmiter object
     */
    public Single<ResultObject> sendSingleMessage(BaseMessage data, Class parseResult) {
        return Single.create(emitter -> emitSingleWebsocketMessage(emitter, data, parseResult));
    }

    /**
     * Send data to websocket and wrap with reactive the response
     * @param emitter SingleEvent emitter
     * @param data Data sent
     */
    private void emitSingleWebsocketMessage(SingleEmitter<ResultObject> emitter, BaseMessage data) {
        singles.put(data.token, emitter);

        websocket
            .send(data)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe();
    }

    /**
     * Send data to websocket and wrap with reactive the response
     * @param emitter SingleEvent emitter
     * @param parser Classe que recebera os dados do result
     * @param data Data sent
     */
    private void emitSingleWebsocketMessage(SingleEmitter<ResultObject> emitter, BaseMessage data, Class parser) {
        singles.put(data.token, emitter);
        singlesParser.put(data.token, parser);

        websocket
            .send(data)
            .observeOn(Schedulers.io())
            .subscribe(
                System.out::println,
                System.out::println
            );
    }

    public void connect() {
        if (websocket == null) return;

        websocket.connect()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                event -> {
                    System.out.println("Conectado com o WebSocket");
                },
                event -> {
                    System.out.println("Erro na conexão com o websocket");
                });
    }

    private void emitSingleMessage(ResultObject res) {
        SingleEmitter<ResultObject> emit = singles.get(res.token);
        Class parser = singlesParser.get(res.token);

        if (res.result != null && parser != null) {
            Gson gson = new Gson();
            res.result = gson.fromJson(gson.toJson(res.result), parser);
        }

        if (res.error) {
            emit.onError(new Exception(res.msg));
        } else {
            emit.onSuccess(res);
        }

        singles.remove(res.token);

        if (singlesParser.get(res.token) != null)
            singlesParser.remove(res.token);
    }

    private void listenToMessage(RxWebsocket.Event event) {
        System.out.printf("Ouvindo mensagem");
        try {
            ResultObject res = ((RxWebsocket.Message) event).data(ResultObject.class);

            // Verifica se a mensagem é do tipo Single
            if (singles.get(res.token) != null) {
                emitSingleMessage(res);
            }

            if (res.broadcast && BroadcastEvent.isListening(res.event)) {
                BroadcastEvent.emitBroadcastMessage(res);
            }
        } catch (Throwable throwable) {
            log("[Mensagem invalida recebida]: " + ((RxWebsocket.Message) event).data().toString());
        }
    }

    private void listen() {
        if (websocket == null) return;

        websocket
            .listen()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(event -> {
                listenToMessage(event);
            });
    }


    private void logError(Throwable throwable) {
        System.out.println(String.format("\n[%s]:[ERROR]%s", getCurrentTime(), throwable.getMessage()));
    }

    private void log(String text) {
        System.out.println(String.format("\n[%s]:%s", getCurrentTime(), text));
    }

    private String getCurrentTime() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        return sdf.format(c.getTime());
    }
}
