package br.com.tcc.safeson.safeson.actions.EventSample;

/**
 * Campos que serão enviados para o servidor ao executar a mensagem EventSample
 */
public class EventSampleMessage {
    private String teste;

    public EventSampleMessage(String teste) {
        this.teste = teste;
    }
}
