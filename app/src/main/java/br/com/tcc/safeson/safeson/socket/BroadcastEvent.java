package br.com.tcc.safeson.safeson.socket;

import com.google.gson.Gson;

import java.util.HashMap;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;

public class BroadcastEvent {
    private static HashMap<String, ObservableEmitter<ResultObject>> broadcasts = new HashMap<>();
    private static HashMap<String, Class> broadcastsParser = new HashMap<>();

    /**
     * Listen to a specific broadcast event
     * @param message Which broadcast event to listen
     * @param parser Result class object
     * @return Observable that handles the event emission
     */
    public static Observable<ResultObject> listen(String message, Class parser) {
        return Observable.create(emitter -> addBroadcastMessage(message, emitter, parser));
    }

    /**
     * Dispose of some broadcast listener
     * @param message
     */
    public static void dispose(String message) {
        broadcasts.get(message).onComplete();
        broadcasts.remove(message);
        broadcastsParser.remove(message);
    }

    /**
     * Emit broadcasted message from event and parse the response
     * @param res Object received from event
     */
    public static void emitBroadcastMessage(ResultObject res) {
        ObservableEmitter<ResultObject> emit = broadcasts.get(res.event);
        Class parser = broadcastsParser.get(res.event);

        if (res.result != null && parser != null) {
            Gson gson = new Gson();
            res.result = gson.fromJson(gson.toJson(res.result), parser);
        }

        if (res.error) {
            emit.onError(new Exception(res.msg));
        } else {
            emit.onNext(res);
        }
    }

    /**
     * Adds broadcast listener and parser to broadcast list
     * @param message
     * @param emitter
     * @param parser
     */
    private static void addBroadcastMessage(String message, ObservableEmitter<ResultObject> emitter, Class parser) {
        broadcasts.put(message, emitter);
        broadcastsParser.put(message, parser);
    }

    /**
     * @param event Name of the event
     * @return If the client is listening to the event
     */
    public static boolean isListening(String event) {
        return broadcasts.get(event) != null;
    }
}
