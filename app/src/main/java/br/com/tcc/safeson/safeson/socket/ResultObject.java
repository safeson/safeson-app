package br.com.tcc.safeson.safeson.socket;

/**
 * Base result object from server
 */
public class ResultObject {
    public boolean error;
    public boolean broadcast;
    public Object result;
    public String msg;
    public String token;
    public String event;

    public ResultObject() {

    }

    public ResultObject(boolean error, String token, Object result, String msg) {
        this.error = error;
        this.token = token;
        this.result = result;
        this.msg = msg;
    }

    public ResultObject(boolean error, String token, Object result) {
        this.error = error;
        this.token = token;
        this.result = result;
    }
}
