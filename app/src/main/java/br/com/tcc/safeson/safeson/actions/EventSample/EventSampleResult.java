package br.com.tcc.safeson.safeson.actions.EventSample;

import java.util.List;

/**
 * Campos que serão retornados do evento EventSample
 */
public class EventSampleResult {
    public List<String> lista;
    public List<Object> objetos;
}

