package br.com.tcc.safeson.safeson.actions;

import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

public class BaseMessage {
    private String action;
    private Date time;
    private Object data;
    public String token;

    public BaseMessage(String action, Object data) {
        this.action = action;
        this.time = Calendar.getInstance().getTime();
        this.data = data;
        this.token = String.valueOf(UUID.randomUUID());
    }

    public BaseMessage(String action, Calendar time, Object data) {
        this.action = action;
        this.time = time.getTime();
        this.data = data;
        this.token = String.valueOf(UUID.randomUUID());
    }
}
